
from h5py import *
from numpy import *

f=File("volumes.h5","r")["org"]
keys=list(f.keys())
keys.sort()
print( keys)
res=[]
for k in keys[1:]:
    res.append(f[k]["scal_prods"]["scalDS"][()])
res=swapaxes(res,0,1)

File("scalpro_by_roi.h5","w")["vol"]  = res

