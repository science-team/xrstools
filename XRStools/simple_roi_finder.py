from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import logging

import numpy as np
import math
import matplotlib.pyplot as plt
import shelve
from matplotlib.path import Path

from matplotlib.widgets import Cursor, Button
from scipy.ndimage import measurements
from scipy import signal, stats, sparse
import copy
from . import xrs_rois, xrs_read

try:
    from skimage import draw

    __have_skimage__ = True
except:
    logging.getLogger(__name__).warning(
        "WARNING: Skimage not available. Lasso selection disabled. You can count on rectangular selection only. . You can install skimage."
    )
    __have_skimage__ = False


import matplotlib

from matplotlib.widgets import RectangleSelector
from matplotlib.widgets import LassoSelector
from matplotlib.widgets import RadioButtons
from matplotlib.widgets import Slider
from matplotlib.widgets import TextBox
from matplotlib import gridspec

# from ixsscan.roi import Roi


class ZoomRoiFinder:
    """Simple ROI finder for rectangular ROIs.

    Parameters:
    -----------
    input_image : np.array (2D)
        Image to use for the selection of the ROI.

    roi : roi.Roi object

    log_scaling : boolean
        Switch if image is to be displayed with logarithmic scaling.
        Default is 'True'.

    cmap : str
        Matplotlib colormap keyword. Default is 'Blues'.

    interpolation : str
        Keyword for matplotlib.imshow. Default is 'nearest'.

    ToDo:
    - use matplotlib RectangleSelector widget
    - implement alteration of input ROI obj.
    - use roi class rectangle definition function

    """

    def reset_roi_obj(self):
        self.roi_obj._reset(self)

    def __init__(self, input_image, roi=None, log_scaling=True, cmap="Blues", interpolation="nearest"):

        if roi is None:
            self.roi_obj = Roi()
        else:
            self.roi_obj = roi

        # check that the input is a 2d matrix
        assert len(input_image.shape) == 2, f"Provided input image is of shape {input_image.shape}, need 2D image to proceed."

        # save input image for later use
        self.input_image = copy.deepcopy(input_image)

        # calculate the logarithm if 'logscaling' == True
        if log_scaling:
            # set all zeros to ones:
            input_image[input_image[:, :] == 0.0] = 1.0
            input_image = np.log(np.abs(input_image))

        # first ROI index
        roi_ind = 0

        # prepare a figure
        fig, ax = plt.subplots()
        plt.subplots_adjust(bottom=0.2)
        cursor = Cursor(ax, useblit=True, color="black", linewidth=1)
        ax.imshow(input_image, interpolation=interpolation, cmap=cmap)

        # no ticks, no labels
        ax.tick_params(left=False, labelleft=False, right=False, labelright=False, bottom=False, top=False, labelbottom=False)
        ax.set_xlim(0.0, input_image.shape[1])
        ax.set_ylim(input_image.shape[0], 0.0)
        title_txt = f"Active ROI is ROI No. {1} of {0}"
        ax.set_title(title_txt)

        # activate the zoom function already
        this_manager = plt.get_current_fig_manager()
        this_manager.toolbar.zoom()

        # plotter
        def plot_all(roi_ind, contour_lines):
            """Function to update plot window.

            Parameters
            ----------
            roi_ind: ROI index (int).

            contour_lines: Dictionary of corners to draw contour lines around ROIs.

            """

            fig.canvas.flush_events()
            ax.cla()

            title_txt = f"Active ROI is ROI No. {roi_ind} of {len(contour_lines)}"

            ax.imshow(input_image, interpolation=interpolation, cmap=cmap)

            # no ticks, no labels on axes
            ax.tick_params(
                left=False, labelleft=False, right=False, labelright=False, bottom=False, top=False, labelbottom=False
            )

            ax.imshow(input_image, interpolation=interpolation, cmap=cmap)
            if contour_lines:
                for key in contour_lines:
                    ax.plot(contour_lines[key][0], contour_lines[key][1], "-k")

            ax.set_xlim(0.0, input_image.shape[1])
            ax.set_ylim(input_image.shape[0], 0.0)
            ax.set_title(title_txt)
            plt.draw()
            plt.pause(0.01)

        class Index(object):
            def __init__(self, red_rois):
                self.red_rois = red_rois
                self.roi_key = "ROI00"
                self.roi_ind = 0
                self.num_ROI_min = 0
                self.contour_lines = {}

            def take_and_next(self, event):

                # grep axis limits
                cur_xlim = ax.get_xlim()
                cur_ylim = ax.get_ylim()

                limits = np.array([np.ceil(cur_xlim[0]), np.floor(cur_xlim[1]), np.floor(cur_ylim[1]), np.ceil(cur_ylim[0])])

                # make sure they are inside the image
                inds = limits < 0
                limits[inds] = 0
                if limits[1] > input_image.shape[1]:
                    limits[1] = input_image.shape[1]
                if limits[2] > input_image.shape[0]:
                    limits[2] = input_image.shape[0]

                print(f"Chosen limits are (x_min, x_max, y_min, y_max): ", limits)

                self.red_rois[self.roi_ind] = (np.array([int(limits[2]), int(limits[0])])), sparse.coo_matrix(
                    np.ones((int(limits[3] - limits[2]), int(limits[1] - limits[0])))
                )

                self.contour_lines[self.roi_ind] = [
                    [limits[0] - 0.5, limits[1] + 0.5, limits[1] + 0.5, limits[0] - 0.5, limits[0] - 0.5],
                    [limits[2] - 0.5, limits[2] - 0.5, limits[3] + 0.5, limits[3] + 0.5, limits[2] - 0.5],
                ]

                plot_all(self.roi_ind, self.contour_lines)

                # go to next roi
                self.roi_ind += 1
                self.roi_key = f"ROI{self.roi_ind:02d}"
                plot_all(self.roi_key, self.contour_lines)

            def middle_mouse_press(self, event):
                if event.button == 2:
                    self.take_and_next(event)

            def take_and_exit(self, event):

                # grep axis limits
                cur_xlim = ax.get_xlim()
                cur_ylim = ax.get_ylim()

                limits = np.array(
                    [np.ceil(cur_xlim[0]), np.floor(cur_xlim[1] + 0.5), np.floor(cur_ylim[1] + 0.5), np.ceil(cur_ylim[0])]
                )

                # make sure they are inside the image
                inds = limits < 0
                limits[inds] = 0
                if limits[1] > input_image.shape[1]:
                    limits[1] = input_image.shape[1]
                if limits[2] > input_image.shape[0]:
                    limits[2] = input_image.shape[0]

                print(f"Chosen limits are (x_min, x_max, y_min, y_max): ", limits)

                self.red_rois[self.roi_ind] = (np.array([int(limits[2]), int(limits[0])])), sparse.coo_matrix(
                    np.ones((int(limits[3] - limits[2]), int(limits[1] - limits[0])))
                )

                self.contour_lines[self.roi_ind] = [
                    [limits[0] + 0.5, limits[1] + 0.5, limits[1] + 0.5, limits[0] + 0.5, limits[0] + 0.5],
                    [limits[2] + 0.5, limits[2] + 0.5, limits[3] + 0.5, limits[3] + 0.5, limits[2] + 0.5],
                ]

                plot_all(self.roi_ind, self.contour_lines)
                plt.close()

            def discard(self, event):
                del self.red_rois[self.roi_key]
                del self.contour_lines[self.roi_key]
                plot_all(self.roi_key, self.contour_lines)

            def prev_roi(self, event):
                if self.roi_ind > self.num_ROI_min:
                    self.roi_ind -= 1
                    self.roi_key = f"ROI{self.roi_ind:02d}"
                    plot_all(self.roi_key, self.contour_lines)
                else:
                    pass

            def next_roi(self, event):
                self.roi_ind += 1
                self.roi_key = f"ROI{self.roi_ind:02d}"
                plot_all(self.roi_key, self.contour_lines)

            def exit(self, event):
                plt.close()

        callback = Index(self.roi_obj._rois)
        
        # buttons for next/prev roi
        axprev_roi = plt.axes([0.58, 0.05, 0.1, 0.075])
        axnext_roi = plt.axes([0.7, 0.05, 0.1, 0.075])
        bnext_roi = Button(axnext_roi, "next ROI")
        bnext_roi.on_clicked(callback.next_roi)
        bprev_roi = Button(axprev_roi, "prev ROI")
        bprev_roi.on_clicked(callback.prev_roi)
        # take and next button
        axtakeN = plt.axes([0.04, 0.05, 0.17, 0.075])
        btakeN = Button(axtakeN, "take+next\n (mouse wheel)")
        btakeN.on_clicked(callback.take_and_next)
        # take and exit
        axtakeE = plt.axes([0.24, 0.05, 0.17, 0.075])
        btakeE = Button(axtakeE, "take+exit")
        btakeE.on_clicked(callback.take_and_exit)
        # discard  button
        axdisc = plt.axes([0.46, 0.05, 0.1, 0.075])
        bdisc = Button(axdisc, "discard")
        bdisc.on_clicked(callback.discard)
        # finish button
        axfin = plt.axes([0.82, 0.05, 0.1, 0.075])
        bfin = Button(axfin, "exit")
        bfin.on_clicked(callback.exit)

        cid = fig.canvas.mpl_connect("button_press_event", callback.middle_mouse_press)

        # show figure
        plt.show()

        # assign rois
        self.roi_obj._rois = callback.red_rois
        self.roi_obj.configure_extraction(restrain_to=None)


class SimpleRoiFinder:
    def __init__(self,  roi=None):

        if roi is None:
            # self.roi_obj = Roi()
            self.roi_obj = xrs_rois.roi_object()            
        else:
            self.roi_obj = roi

    def to_roi(self, event):
        self.roi_obj.mask_into_rois(self.mask)

    def cancel_roi(self, event):
        ID = self.roi_id_slider.val
        self.mask[self.mask == ID] = np.nan
        self.mask_figure_obj.remove()
        self.mask_figure_obj = self.ax_imshow.imshow(
            (np.minimum(0, self.mask) + 1) * self.input_image, "jet", interpolation=self.interpolation, alpha=0.5
        )
        self.annotate_spots()
        self.fig_imshow.canvas.draw()
        self.fig_imshow.canvas.flush_events()


    def prepare_image(self,
                      exp_dir = "/data/run7_ihr_gz/hydra",
                      scans = [592, 593, 594, 595, 596, 597, 598, 599] ,
                      monitorcolumn = 'kapraman'
    ):
        experiment = xrs_read.read_id20( exp_dir ,monitorcolumn=monitorcolumn)
        edfmat =  experiment.read_just_first_scanimage(scans[0])
        
        image4roi =  experiment.SumDirect( scans )
        
        return image4roi
        
    
        
    def gui_session(self, input_image, modind=-1, logscaling=True, colormap="Blues", interpolation="nearest", forbidden_mask = None):
        self.forbidden_mask = forbidden_mask 
        # check that the input is a 2d matrix
        if not len(input_image.shape) == 2:
            print("Please provide a 2D numpy array as input!")
            return
        # calculate the logarithm if 'logscaling' == True
        if logscaling:
            # set all zeros to ones:
            input_image[input_image[:, :] == 0.0] = 1.0
            input_image = np.log(np.abs(input_image))

        self.roi_obj.input_image = input_image
        self.mask = self.roi_obj.get_rois_as_mask(input_image.shape)
        self.input_image = input_image
        self.interpolation = interpolation

        #  FIG 1 ( fig_imageshow with axis ax_imshow)
        fig_imshow, ax_imshow = plt.subplots()
        plt.subplots_adjust(bottom=0.05, top=1, left=0.05, right=1)

        # FIG2
        fig2 = plt.figure()
        axcolor = "lightgoldenrodyellow"
        ax_which_action = plt.axes([0.05, 0.7, 0.15, 0.15], facecolor=axcolor)
        ax_which_selection = plt.axes([0.3, 0.7, 0.15, 0.15], facecolor=axcolor)
        ax_suppr = plt.axes([0.05, 0.4, 0.15, 0.15], facecolor=axcolor)
        ax_save = plt.axes([0.3, 0.4, 0.15, 0.15], facecolor=axcolor)
        ax_slider = plt.axes([0.1, 0.05, 0.8, 0.05], facecolor=axcolor)
        titlestring = ""
        titleInst = plt.suptitle(titlestring)

        cursor = Cursor(ax_imshow, useblit=True, color="red", linewidth=1)

        # generate an image to be displayed
        figure_obj = ax_imshow.imshow(input_image, interpolation=interpolation)

        # set the colormap for the image
        figure_obj.set_cmap(colormap)

        self.mask_figure_obj = ax_imshow.imshow(
            (np.minimum(0, self.mask) + 1) * input_image, "jet", interpolation=interpolation, alpha=0.5
        )

        ## ~~~~~~~~~~~~~~~~~~ Button Cancel Roi ~~~~~~~~~~~~~~~~~~~~~~~~~
        self.b_cancel = Button(ax_suppr, "Suppr.", color=axcolor, hovercolor="red")
        self.b_cancel.on_clicked(self.cancel_roi)
        # ----------------------------------------------------------

        ## ~~~~~~~~~~~~~~~~~~ Button To Roi ~~~~~~~~~~~~~~~~~~~~~~~~~
        self.b_save = Button(ax_save, "To Roi Obj", color=axcolor, hovercolor="red")
        self.b_save.on_clicked(self.to_roi)
        # ----------------------------------------------------------

        ## ~~~~~~~~~~~~~~~~~~~~~~ which Action  ~~~~~~~~~~~~~~~~~~~~
        axcolor = "lightgoldenrodyellow"
        rax = plt.axes(ax_which_action, facecolor=axcolor)
        radio_which_action = RadioButtons(rax, ("add", "subtract"))

        def select_action(label):
            self.action = label
            plt.draw()

        radio_which_action.on_clicked(select_action)
        self.radio_which_action = radio_which_action
        # ---------------------------------------------------------------------

        ## ~~~~~~~~~~~~~~~~~~~~~~ which selection type ~~~~~~~~~~~~~~~~~~~~
        axcolor = "lightgoldenrodyellow"
        rax = plt.axes(ax_which_selection, facecolor=axcolor)
        radio_which_selection_type = RadioButtons(rax, ("None", "Lasso", "Rectangle"))

        def select_selection(label):
            if label == "Lasso" and not __have_skimage__:
                logging.getLogger(__name__).warning(
                    "WARNING: Skimage.draw not available. Lasso selector cannot work. You can install skimage"
                )
                radio_which_selection_type.set_active(0)
                return
            self.lasso_selection.set_connection(label == "Lasso")
            self.rectangle_selection.set_connection(label == "Rectangle")
            return

        radio_which_selection_type.on_clicked(select_selection)
        # ---------------------------------------------------------------------

        roi_id_slider = Slider(ax_slider, "ID", 0, 72, valinit=0, valstep=1)

        self.roi_id_slider = roi_id_slider

        self.fig_imshow = fig_imshow
        self.ax_imshow = ax_imshow
        self.annotations = []

        self.lasso_selection = HandSelection(axes=ax_imshow, selection_type="Lasso", gui_session=self)
        self.rectangle_selection = HandSelection(axes=ax_imshow, selection_type="Rectangle", gui_session=self)

        self.annotate_spots()
        plt.show()

    def on_selection_event(self, selection_type=None, args=None):
        """this method is meant to be called by HandSelection objects"""

        ID = self.roi_id_slider.val

        if selection_type == "Lasso":
            (points,) = args
            rr, cc = draw.polygon(np.array(points)[:, 1].astype(np.int), np.array(points)[:, 0].astype(np.int))

            if len(rr) == 0 or len(cc) == 0:
                return
            else:
                if self.radio_which_action.value_selected == "add":
                    self.mask[rr, cc] = ID
                else:
                    self.mask[rr, cc] = math.nan

        else:
            start, end = args
            x1, y1, x2, y2 = map(int, (start.xdata, start.ydata, end.xdata, end.ydata))
            x1, x2 = sorted((x1, x2))
            y1, y2 = sorted((y1, y2))

            if self.radio_which_action.value_selected == "add":
                self.mask[y1:y2, x1:x2] = ID
            else:
                self.mask[y1:y2, x1:x2] = math.nan

        if  self.forbidden_mask is not None:
            self.mask[ self.forbidden_mask==1 ] = math.nan
                
        self.mask_figure_obj.remove()
        self.mask_figure_obj = None

        self.fig_imshow.canvas.draw()
        self.fig_imshow.canvas.flush_events()

        #  self.gs.mask_figure_obj = self.current_ax.imshow( (1-np.isnan(self.gs.mask))*self.gs.input_image   , 'jet', interpolation=self.gs.interpolation, alpha=0.5)
        self.mask_figure_obj = self.ax_imshow.imshow(
            (np.minimum(0, self.mask) + 1) * self.input_image, "jet", interpolation=self.interpolation, alpha=0.5
        )
        self.annotate_spots()
        self.fig_imshow.canvas.draw()
        self.fig_imshow.canvas.flush_events()

    def annotate_spots(self):
        while self.annotations:
            ann = self.annotations.pop()
            ann.remove()

        tmp_mask = np.copy(self.mask)
        tmp_mask[np.isnan(tmp_mask)] = -1
        nspots = int(tmp_mask.max()) + 1

        for i in range(0, nspots):
            m = (self.mask == i).astype("f")
            msum = m.sum()
            if msum:
                ny, nx = m.shape
                px = (m.sum(axis=0) * np.arange(nx)).sum() / msum
                py = (m.sum(axis=1) * np.arange(ny)).sum() / msum

                info = "(#%d)" % i
                self.annotations.append(
                    self.ax_imshow.annotate(
                        info,
                        xy=(px, py),
                        xycoords="data",
                        xytext=(-20, 20),
                        textcoords="offset pixels",
                        horizontalalignment="right",
                        verticalalignment="bottom",
                        bbox=dict(boxstyle="round,pad=0.5", fc="yellow", alpha=0.4),
                        arrowprops=dict(arrowstyle="->", connectionstyle="arc3,rad=0"),
                    )
                )


class HandSelection(object):
    def __init__(self, axes=None, selection_type=None, gui_session=None):
        self.current_ax = axes
        self.selection_type = selection_type

        self.gs = gui_session

        self.selector = None

    def onselect(self, *args):
        self.gs.on_selection_event(selection_type=self.selection_type, args=args)

    def connect(self):
        if self.selection_type == "Lasso":
            self.selector = LassoSelector(self.current_ax, onselect=self.onselect)
        elif self.selection_type == "Rectangle":
            self.selector = RectangleSelector(self.current_ax, onselect=self.onselect)

    def set_connection(self, value):
        if not value:
            if self.selector is not None:
                self.selector.disconnect_events()
                self.selector = None
        else:
            self.connect()



            
