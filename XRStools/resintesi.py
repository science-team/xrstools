from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
from scipy import interpolate, signal, integrate, constants, optimize, ndimage

def FandGrad( Y, factors,  base_functs  ):
    simu = np.dot( factors, base_functs  )
    diff = -Y+simu
    error = 0.5*(diff*diff).sum()
    
    grad  = np.dot(  base_functs,  diff ) 
    
    return error, grad

def resintetizza(  Xexp, Yexp,  XY  ):

    X,Y = XY

    norm_Yexp = Yexp.max()
    norm_Y    = Y   .max()
    
    Yexp = Yexp / norm_Yexp
    Y    = Y    / norm_Y

    base_shifts_facts = []
    base_functs = []

    mpos_XY = X[np.argmax(Y)]

    for eX in Xexp:
        base_functs.append(      np.interp( Xexp + mpos_XY -eX,
                                            X,
                                            Y)   )
    base_functs = np.array(base_functs)
        
    factors = np.ones_like( Xexp)
    for iter in range(100):

        factors = factors / np.linalg.norm(factors)
        
        error, grad = FandGrad( Yexp, factors,  base_functs  )

        factors = grad

        Lip =  abs(np.linalg.norm(factors))

        print("Lipschitz = ", Lip)

        
    factors = np.zeros_like( Xexp)
    factors_old = factors
    
    t=1.0
    told = t
    
    for iter in range(10000):
        error, grad = FandGrad( Yexp, factors,  base_functs  )

        # factors = factors-1000000000.0/Lip
        
        factors = np.maximum(0,factors-grad/Lip)

        told = t
        t  = (1+np.sqrt(1+4*t*t))/2

        step = (factors-factors_old)*(told-1)/t
        
        factors_old = factors
        
        factors = factors + step
        
        print(" error ", error ) 

        
    return factors * norm_Yexp / norm_Y

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


def get_resynth( STW_infos, synth_coeffs , Xexp):
    
    todo_list =  { "RC_curve", "A_curve", "B_curve", "C_curve" }
    result = {}


    synth_coeffs = synth_coeffs/np.sum( synth_coeffs  )
    

    X, Y = STW_infos["RC_curve"]
    mpos_XY = X[np.argmax(Y)]

    e_baricenter = (synth_coeffs*Xexp).sum()/synth_coeffs.sum()
    
    for  todo in todo_list:
        resintesi = 0
        dum, Y = STW_infos[todo]

        
        for eX, f  in zip(Xexp, synth_coeffs):
            
            ##  resintesi = resintesi + f*  np.interp( X + mpos_XY -eX, X, Y)
            resintesi = resintesi + f*  np.interp( X + (e_baricenter -eX), X, Y)

        result[todo] = np.array([ X, resintesi   ] )
        
    return result 
        

def operator_norm(linop, u, maxiter=100):
    u = u /  np.linalg.norm(u)
    for i in range(maxiter):
        v = linop(u)
        L=(u[:] * v[:]).sum()
        u = v / np.linalg.norm(v[:])
    return L

def CP(prox_fs, prox_g, K, KT,  x0,   maxiter=100,  error=None):
    """ This routine has been copied from  pyprox project by  Samuel Vaiter <samuel.vaiter@ceremade.dauphine.fr>
    https://github.com/svaiter/pyprox

    References
    ----------
    A. Chambolle and T. Pock,
    A First-Order Primal-Dual Algorithm for Convex Problems
    with Applications to Imaging,
    JOURNAL OF MATHEMATICAL IMAGING AND VISION
    Volume 40, Number 1 (2011)
    """

    theta = 1
    L = operator_norm(
        lambda x: KT(K(x)),
        np.random.random(x0.shape)
        )
    sigma = 1.0/np.sqrt(L)
    tau = .9 / (sigma * L)

    x = x0.copy()
    x1 = x0.copy()
    xold = x0.copy()
    y = K(x)
    fx = []
    iterations = 1
    while iterations < maxiter:
        xold = x.copy()
        y = prox_fs(y + sigma * K(x1), sigma)
        x = prox_g(x - tau * KT(y), tau)
        x1 = x + theta * (x - xold)
        if error is not None:
            fx.append(error(x))
            print(" CP iter ", iterations, " " , fx[-1])
        iterations += 1
    return x, fx


def gradient(x):
    g = np.zeros_like(x)
    g [ :-1 ] = x [ 1: ] - x [ :-1 ]
    return g

def gradientT(p):
    res = np.zeros_like(p)
    res[1:  ] =      p [ :-1 ]
    res[ :-1] = res[ :-1] - p [ :-1 ]
    return res



# Minimization of F(x) + G(gradient*x)
class G_functor:
    def __init__(self, alpha):
        self.alpha = alpha
    def __call__(self,u):
        return self.alpha * np.sum(np.abs(u))


class F_functor:
    def __init__(self, Yexp, base_functs):
        self.Yexp = Yexp
        self.base_functs = base_functs
        
    def __call__(self, factors):
        simu = np.dot( factors, self.base_functs  )
        diff = -self.Yexp+simu
        error = 0.5*(diff*diff).sum()
        return error
    
        ## grad  = np.dot(  base_functs,  diff ) 

class Error_functor:
    def __init__(self, F, G):
        self.F = F
        self.G = G
    def __call__(self, x):
        error = self.F(x) + self.G(gradient(x))
        print(" Error CP", error )
        return error


class K_functor:
    def __init__(self, base_functs):
        self.base_functs = base_functs
    def __call__(self,x):
        g  = gradient(x)
        gp = np.dot( x, self.base_functs  )
        
        res = np.concatenate([g,gp], axis=0)
        return res
    
class KT_functor:
    def __init__(self, base_functs):
        self.base_functs = base_functs
    def __call__(self,P):
        N = self.base_functs.shape[0]
        px = P[:N]
        res = gradientT(px)
        py = P[N:]
        res = res + np.dot(   self.base_functs    , py)
        return res

def prox_f( x,tau):
    return x

class prox_gs_functor:
    def __init__(self, alpha,data, N):
        self.alpha = alpha
        self.data  = data
        self.N = N

    def __call__( self, P, tau ):
        print(" P size ", P.shape , " e N ", self.N)
        res=np.zeros_like(P)

        px=P[:self.N]    
        py = P[self.N:]

        res[:self.N] = self.alpha*px/(np.maximum(self.alpha,np.abs(px)))
        res[self.N:] = (py - tau*self.data)/(1+tau)
        return res


def resintetizzaTV(  Xexp, Yexp,  XY , beta ):
    
    X,Y = XY

    norm_Yexp = Yexp.max()
    norm_Y    = Y   .max()
    
    Yexp = Yexp / norm_Yexp
    Y    = Y    / norm_Y
    
    base_shifts_facts = []
    base_functs = []

    mpos_XY = X[np.argmax(Y)]

    for eX in Xexp:
        base_functs.append(      np.interp( Xexp + mpos_XY -eX,
                                            X,
                                            Y)   ) 
    base_functs = np.array(base_functs)

    factors = np.ones_like( Xexp)

    N = Yexp.shape[0]
    prox_gs = prox_gs_functor(beta, Yexp,N  )
    
    K  =  K_functor(  base_functs  )
    KT = KT_functor(  base_functs  )


    G = G_functor(  beta ) 
    F = F_functor( Yexp, base_functs ) 
    
    Error = Error_functor( F,G )
    
    sol, fs = CP(prox_gs, prox_f, K, KT,  factors ,   maxiter=1000,  error=Error)
    print( " USCITO da CP ") # , fs)
    return sol
