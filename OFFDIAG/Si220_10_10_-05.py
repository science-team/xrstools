from XRStools import resintesi

import matplotlib
matplotlib.use('Qt4Agg')

# from silx import sx
from silx.gui.plot import Plot2D
from silx.gui import qt
qapp = qt.QApplication([])

    
from XRStools import xrs_read, theory, xrs_extraction, xrs_rois, roifinder_and_gui, xrs_utilities, math_functions, roiSelectionWidget, Bragg
from pylab import *

# ion()
import numpy as np
from scipy import interpolate, signal, integrate, constants, optimize, ndimage
from XRStools import ixs_offDiagonal,xrs_utilities

import h5py

##############################################################################
# off-focus resolution tests
##############################################################################

repertorio = "/data/id20/inhouse/data/run5_15/run6_hc2251"
repertorio = "/olddisk/data/id20/inhouse/data/run5_15/run6_hc2251"

############
# off-dia terzo: G=[-1,-1,-1], q0=[1.0, 1.0, -0.5], q1=[0.0, 0.0, -1.5]
############

#
#
# the tthh angle was POS. 21.18 degrees, which is the
# correct angle for the primo crystal!!!!!!!!!!!!!!
#
# BUT terzo crystal was used !!!!!!!!!!!!!!!!!!!!!
#
# this point may be not valid !!!!!!!!!!!!!!!!!!!!
#

from XRStools import ixs_offDiagonal
offdia_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column='kaprixs',   SPECfname='fourc_220',  EDFname='fourc_220_'    )

print(" OK ")

if 0:
    image4roi =  offdia_ixsobj.SumDirect( [1] )
    w4r = roiSelectionWidget.launch4MatPlotLib(im4roi=image4roi, layout = "1X5-1" )
    raise
    
roifinder = roifinder_and_gui.roi_finder()

# roifinder.get_zoom_rois(image4roi)

#roifinder.roi_obj.writeH5('/data/id20/inhouse/data/off_dia_jan18/rois/ROI_Si111_q0_10_10_-05.h5')
# roifinder.roi_obj.loadH5('/data/id20/inhouse/data/off_dia_jan18/rois/ROI_Si111_q0_10_10_-05.h5')

asymm_grad       =  5.0   
tthv_grad        =  23.9
tth_grad         = 7.5
psi_grad =  -40.3
source_distance = 60.0
magnification = 0.11
E0 = 7950.0
hkl_mono =  np.array(  [ 1,1,1  ])
hkl =  np.array(  [ 2,2,0 ])
Hw = 0.5 # millimetri
DeltaAngle =0.00000*180/np.pi

# STW_infos =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl, Hw = Hw, asymm_grad = asymm_grad,
#                                           tthv_grad=tthv_grad, tth_grad=tth_grad, psi_grad = psi_grad,
#                                           source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle)


#np.savetxt("monochromator_response_energy_111.txt",STW_infos["RC_curve"].T)

# np.savetxt("rocking_curve.txt",rocking_curve.T)


roifinder.roi_obj.loadH5("ROI_Si220_q0_10_10_-05.h5:/roi_from_selector")
offdia_ixsobj.set_roiObj(roifinder.roi_obj)
# plasmon and core part
if(0):
    offdia_ixsobj.loadRockingCurve(list(range(4,525,4)),direct=True, storeInsets = True)
    offdia_ixsobj.save_state_hdf5(  "offdia_ixsobj_220.h5", "after_loading", comment="" , overwriteFile = True,  overwriteGroup=True)
    raise " OK "
else:
    if(1):   
        print(" RELOADING " )
        offdia_ixsobj.load_state_hdf5(  "offdia_ixsobj_220.h5", "after_loading",)
        print(" RELOADING OK" )
        print("    stitchRockingCurves ")
        offdia_ixsobj.stitchRockingCurves(I0moni='kaprixs',RCmoni='alirixs') 
        print("    stitchRockingCurves OK ")
        
        # ROI1
        
        offdia_ixsobj.offDiaDataSets[0].filterDetErrors(threshold=0.95*np.amax(offdia_ixsobj.offDiaDataSets[0].signalMatrix))
        offdia_ixsobj.offDiaDataSets[0].normalizeSignals()
        #offdia_ixsobj.offDiaDataSets[0].alignRCmonitorCC(repeat=2)    
        # ROI2
        offdia_ixsobj.offDiaDataSets[1].filterDetErrors(threshold=0.95*np.amax(offdia_ixsobj.offDiaDataSets[1].signalMatrix))
        offdia_ixsobj.offDiaDataSets[1].normalizeSignals()  ##  tutti i segnali all'interno di una roi

        #offdia_ixsobj.offDiaDataSets[1].deglitchSignalMatrix(110,1140,10000)
        #offdia_ixsobj.offDiaDataSets[1].alignRCmonitorCC(repeat=2)
        # ROI3
        offdia_ixsobj.offDiaDataSets[2].filterDetErrors(threshold=0.95*np.amax(offdia_ixsobj.offDiaDataSets[2].signalMatrix))
        offdia_ixsobj.offDiaDataSets[2].normalizeSignals()


        ####################################################################################"
        STW_infos =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl , hkl_mono = hkl_mono , Hw = Hw*0.00001, asymm_grad = asymm_grad,
                                                  tthv_grad=tthv_grad, tth_grad=tth_grad, psi_grad = psi_grad,
                                                  source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle)
        

        STW_infos_nowidth =    Bragg.calculate_RC_factors(E0 = E0 , hkl=hkl , hkl_mono = hkl_mono , Hw = Hw*0.00001, asymm_grad = asymm_grad,
                                                  tthv_grad=tthv_grad, tth_grad=tth_grad, psi_grad = psi_grad,
                                                  source_distance = source_distance, magnification=magnification , DeltaAngle = DeltaAngle)


        
        offdia_ixsobj.offDiaDataSets[0].alignRCmonitor( STW_infos["RC_curve"]  )

        synth_coeffs= resintesi.resintetizza(  offdia_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0,   
                                                offdia_ixsobj.offDiaDataSets[0].alignedRCmonitor.mean(axis=0),
                                                STW_infos_nowidth["RC_curve"]  )
        
        STW_infos    = resintesi.get_resynth_coeffs( STW_infos_nowidth, synth_coeffs,  offdia_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0 )

        np.savetxt("xy220.txt", np.array( [   offdia_ixsobj.offDiaDataSets[0].masterRCmotor   * np.pi/180.0 ,
                                            offdia_ixsobj.offDiaDataSets[0].alignedRCmonitor.mean(axis=0)]).T 
        )
        np.savetxt("synt_coeffs220.txt", np.array( [   offdia_ixsobj.offDiaDataSets[0].masterRCmotor  * np.pi/180.0 ,
                                                     synth_coeffs]).T
        )
        np.savetxt("rocking_curve_nowidth220.txt",STW_infos_nowidth["RC_curve"].T)
                   
        np.savetxt("rocking_curve220.txt",STW_infos["RC_curve"].T)

        raise

    
        offdia_ixsobj.offDiaDataSets[1].alignRCmonitor( STW_infos["RC_curve"]    )        
        offdia_ixsobj.offDiaDataSets[2].alignRCmonitor( STW_infos["RC_curve"]     )

        np.savetxt("rocking_curve220.txt",STW_infos["RC_curve"].T)

        
        #offdia_ixsobj.offDiaDataSets[2].alignRCmonitorCC(repeat=2)
        
        offdia_ixsobj.save_state_hdf5(  "offdia_ixsobj_aligned_bis_220.h5", "after_stichting_aligning")


        raise "OK"

    else:
        print( " RELOADING OFFDIAG " )
        offdia_ixsobj.load_state_hdf5(  "offdia_ixsobj_aligned.h5", "after_stichting_aligning")



if(0):        
   #ion()
   plot = Plot2D()  # Create the plot widget
   plot.addImage(offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix, legend='image')
   plot.show()
   qapp.exec_()

   imshow(np.log(offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix))
   show()
   qapp.exec_()



if 1:
    # problem with data point 32:
    for ii in list(range(21)):
        offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[32,ii] = np.mean( (offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[31,ii],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[33,ii])  )

    # problem with data point 80:
    for ii in list(range(21)):
        offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[80,ii] = np.mean( (offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[79,ii],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[81,ii])  )




##############
# diagonal
##############
dia_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column='kaprixs')

dia_ixsobj.set_roiObj(roifinder.roi_obj)
# plasmon and core part

if 0 :
    print(" loadRockCurve Diag ")
    dia_ixsobj.loadRockingCurve(list(range(2939,3460,4)), direct=True)
    
    print(" Stitch Diag ")
    
    dia_ixsobj.stitchRockingCurves(I0moni='kaprixs',RCmoni='alirixs')

    dia_ixsobj.save_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting")
    
    # ROI1
    dia_ixsobj.offDiaDataSets[0].filterDetErrors(threshold=0.99*np.amax(offdia_ixsobj.offDiaDataSets[0].signalMatrix))
    dia_ixsobj.offDiaDataSets[0].normalizeSignals()
    dia_ixsobj.offDiaDataSets[0].alignRCmonitor()
    
    # ROI2
    dia_ixsobj.offDiaDataSets[1].filterDetErrors(threshold=0.99*np.amax(offdia_ixsobj.offDiaDataSets[0].signalMatrix))
    dia_ixsobj.offDiaDataSets[1].normalizeSignals()
    dia_ixsobj.offDiaDataSets[1].alignRCmonitor()
    
    # ROI3
    dia_ixsobj.offDiaDataSets[2].filterDetErrors(threshold=0.99*np.amax(offdia_ixsobj.offDiaDataSets[0].signalMatrix))
    dia_ixsobj.offDiaDataSets[2].normalizeSignals()
    dia_ixsobj.offDiaDataSets[2].alignRCmonitor()
    
    dia_ixsobj.save_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting_aligning")

else:
    print( " RELOADING DIAG " )
    dia_ixsobj.load_state_hdf5(  "dia_ixsobj_aligned.h5", "after_stichting_aligning")


if 1:
    # problem with data point 32:
    for ii in list(range( dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix.shape[1]  )):
        dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[32,ii] = np.mean( (dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[31,ii],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[33,ii])  )

    # problem with data point 80:
    for ii in list(range(  dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix.shape[1]   )):
        dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[80,ii] = np.mean( (dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[79,ii],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[81,ii])  )


    

    

#############################
# background for off-diagonal
#############################
back_ixsobj = ixs_offDiagonal.offDiagonal(repertorio ,en_column='sry', moni_column='kaprixs')

backroifinder = roifinder_and_gui.roi_finder()
#backroifinder.get_zoom_rois(image4roi)

#backroifinder.roi_obj.writeH5('/data/id20/inhouse/data/off_dia_jan18/rois/back_ROI_Si111_q0_10_10_-05.h5')
backroifinder.roi_obj.loadH5('back_ROI_Si111_q0_10_10_-05.h5')

back_ixsobj.set_roiObj(backroifinder.roi_obj)
# plasmon and core part

if 0 :
    back_ixsobj.loadRockingCurve(list(range(2937,3458,4)), direct=True)
    
    back_ixsobj.stitchRockingCurves(I0moni='kaprixs',RCmoni='alirixs')
    
    # ROI1
    back_ixsobj.offDiaDataSets[0].filterDetErrors(threshold=0.99*np.amax(back_ixsobj.offDiaDataSets[0].signalMatrix))
    back_ixsobj.offDiaDataSets[0].normalizeSignals()
    back_ixsobj.offDiaDataSets[0].alignRCmonitor()
    
    # ROI2
    back_ixsobj.offDiaDataSets[1].filterDetErrors(threshold=0.99*np.amax(back_ixsobj.offDiaDataSets[0].signalMatrix))
    back_ixsobj.offDiaDataSets[1].normalizeSignals()
    back_ixsobj.offDiaDataSets[1].alignRCmonitor()
    
    # ROI3
    back_ixsobj.offDiaDataSets[2].filterDetErrors(threshold=0.99*np.amax(back_ixsobj.offDiaDataSets[0].signalMatrix))
    back_ixsobj.offDiaDataSets[2].normalizeSignals()
    back_ixsobj.offDiaDataSets[2].alignRCmonitor()

    back_ixsobj.save_state_hdf5(  "back_ixsobj_aligned.h5", "after_stichting_aligning")


    
    # replace background signals by constant fit through signals
    back_ixsobj.offDiaDataSets[0].replaceSignalByConstant([7.95,10.0])
    back_ixsobj.offDiaDataSets[1].replaceSignalByConstant([7.95,10.0])
    back_ixsobj.offDiaDataSets[2].replaceSignalByConstant([7.95,10.0])

    back_ixsobj.save_state_hdf5(  "back_ixsobj_aligned_rep.h5", "after_stichting_aligning_replacing")

else:

    print( " RELOADING BACK " )
    back_ixsobj.load_state_hdf5(  "back_ixsobj_aligned_rep.h5", "after_stichting_aligning_replacing")
    

f=h5py.File("confronti.h5","w")
g0=f.require_group("D0" )
g0["off"]  = offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix
g0["back"] = back_ixsobj.offDiaDataSets[0].alignedSignalMatrix
g0["diff"] =     offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix - 1.3*back_ixsobj.offDiaDataSets[0].alignedSignalMatrix
    
g1=f.require_group("D1" )
g1["off"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g1["back"] = back_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g1["diff"] =     offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix - 1.15*back_ixsobj.offDiaDataSets[1].alignedSignalMatrix

g2=f.require_group("D2" )
g2["off"]  = offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix
g2["back"] = back_ixsobj.offDiaDataSets[2].alignedSignalMatrix
g2["diff"] =     offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix - 1.55*back_ixsobj.offDiaDataSets[2].alignedSignalMatrix
f.close()

# ion()
# imshow(np.log(back_ixsobj.offDiaDataSets[1].alignedSignalMatrix))

#######################################
# subtract background from off-diagonal
#######################################
# ion()
# imshow(np.log(offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix-back_ixsobj.offDiaDataSets[1].alignedSignalMatrix*1.15))

scale = 1.3
offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[0].alignedSignalMatrix*scale

scale = 1.15
offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[1].alignedSignalMatrix*scale

scale = 1.55
offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix -= back_ixsobj.offDiaDataSets[2].alignedSignalMatrix*scale

#######################################
# window
#######################################

# ii=9
# plot(np.array(offdia_ixsobj.offDiaDataSets[1].energy),offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[:,ii])

# ROI1


f=h5py.File("confronti_L23.h5","w")
goff = f.require_group("off" )
g0=goff.require_group("prima_sub" )
g0["signal_prima"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix

subtract = np.zeros_like( offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix) 

# try subtraction the SiL23-pre-edge region to zero OFF-dia
for ii in range(len(offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[0,:])):
        inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[1].energy) >= 8.015,np.array(offdia_ixsobj.offDiaDataSets[1].energy) <= 8.022))[0]
        back = np.polyval(np.polyfit(np.array(offdia_ixsobj.offDiaDataSets[1].energy)[inds],offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,ii],0),np.array(offdia_ixsobj.offDiaDataSets[1].energy))
        subtract[inds,ii]  = back[inds]
        offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[:,ii] -=back

g0["subtract"]     = subtract
g0["signal_dopo"]  = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g0["energy"]       = offdia_ixsobj.offDiaDataSets[1].energy


gdia = f.require_group("dia" )
g0=gdia.require_group("prima_sub" )
g0["signal_prima"]  = dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix

subtract = np.zeros_like( offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix) 
     

# try subtraction the SiL23-pre-edge region to zero DIA
for ii in range(len(dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[0,:])):
        inds = np.where(np.logical_and(np.array(dia_ixsobj.offDiaDataSets[1].energy) >= 8.015,np.array(dia_ixsobj.offDiaDataSets[1].energy) <= 8.022))[0]
        back = np.polyval(np.polyfit(np.array(dia_ixsobj.offDiaDataSets[1].energy)[inds],dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,ii],0),np.array(dia_ixsobj.offDiaDataSets[1].energy))
        subtract[inds,ii]  = back[inds]
        dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[:,ii] -=back

g0["subtract"]     = subtract
g0["signal_dopo"]  = dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix
g0["energy"]       = dia_ixsobj.offDiaDataSets[1].energy


f.close()


E0 = 7.92515
inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[1].energy)>=7.927, np.array(offdia_ixsobj.offDiaDataSets[1].energy)<=8.020))[0]
SVDMatrix1 = offdia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,:]
diaMatrix1 = dia_ixsobj.offDiaDataSets[1].alignedSignalMatrix[inds,:]
energy1    = (np.array(offdia_ixsobj.offDiaDataSets[1].energy)[inds] - E0)*1e3

SVDMatrix1 = np.insert(SVDMatrix1,0,diaMatrix1[:,0],axis=1)
SVDMatrix1 = np.insert(SVDMatrix1,-0,diaMatrix1[:,1],axis=1)
      
# +++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[0].energy)>=7.929, np.array(offdia_ixsobj.offDiaDataSets[0].energy)<=8.020))[0]
# SVDMatrix0 = offdia_ixsobj.offDiaDataSets[0].alignedSignalMatrix[inds,:]
# diaMatrix0 = dia_ixsobj.offDiaDataSets[0].alignedSignalMatrix[inds,:]
# energy0    = (np.array(dia_ixsobj.offDiaDataSets[0].energy)[inds] - E0)*1e3

# SVDMatrix0 = np.insert(SVDMatrix0,0,diaMatrix0[:,0],axis=1)
# SVDMatrix0 = np.insert(SVDMatrix0,-0,diaMatrix0[:,1],axis=1)

# ion()
# imshow(SVDMatrix0)

# # ROI 2
# E0 = 7.92525
# # try subtraction the SiL23-pre-edge region to zero OFF-dia
# for ii in range(len(offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[0,:])):
# 	inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[2].energy) >= 8.015,np.array(offdia_ixsobj.offDiaDataSets[2].energy) <= 8.022))[0]
# 	back = np.polyval(np.polyfit(np.array(offdia_ixsobj.offDiaDataSets[2].energy)[inds],offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[inds,ii],0),np.array(offdia_ixsobj.offDiaDataSets[2].energy))
# 	offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[:,ii] -=back

# # try subtraction the SiL23-pre-edge region to zero DIA
# for ii in range(len(dia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[0,:])):
# 	inds = np.where(np.logical_and(np.array(dia_ixsobj.offDiaDataSets[2].energy) >= 8.015,np.array(dia_ixsobj.offDiaDataSets[2].energy) <= 8.022))[0]
# 	back = np.polyval(np.polyfit(np.array(dia_ixsobj.offDiaDataSets[2].energy)[inds],dia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[inds,ii],0),np.array(dia_ixsobj.offDiaDataSets[2].energy))
# 	dia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[:,ii] -=back

# inds = np.where(np.logical_and(np.array(offdia_ixsobj.offDiaDataSets[2].energy)>=7.928, np.array(offdia_ixsobj.offDiaDataSets[2].energy)<=8.020))[0]
# SVDMatrix2 = offdia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[inds,:]
# diaMatrix2 = dia_ixsobj.offDiaDataSets[2].alignedSignalMatrix[inds,:]
# energy2    = (np.array(dia_ixsobj.offDiaDataSets[2].energy)[inds] - E0)*1e3

# SVDMatrix2 = np.insert(SVDMatrix2,0,diaMatrix2[:,0],axis=1)
# SVDMatrix2 = np.insert(SVDMatrix2,-0,diaMatrix2[:,1],axis=1)

# ion()
# imshow(SVDMatrix2)


#######################################
# decompose using SVD after metere
#######################################

#U0,S0,V0 = np.linalg.svd(SVDMatrix0)

U1,S1,V1 = np.linalg.svd(SVDMatrix1)

#U2,S2,V2 = np.linalg.svd(SVDMatrix2)

# tthv, tthh, psi = xrs_utilities.cixsUBgetAngles_primo([1.,1.,0.5])
# Q, K0, K2 = xrs_utilities.cixsUBgetQ_primo(tthv, tthh, psi)
# G = [-1.,-1.,-1.]
# Kh = G+K0

# e0 = K0/np.linalg.norm(K0)
# eh = Kh/np.linalg.norm(Kh)
# e2 = K2/np.linalg.norm(K2)

# pre_factors = np.array([ (e0.dot(e2))**2, (eh.dot(e2))**2, 2.0*(e0.dot(e2))*(eh.dot(e2)), 2.0*(e0.dot(e2))*(eh.dot(e2))  ])

# forx   = np.loadtxt('/media/christoph/Seagate/data/off_dia_jan18/wfc/Si111_alpha12.0_z0.01.dat')
# cenofx = xrs_utilities.fwhm(forx[:,0], forx[:,5])[1]
# forx[:,0] -= cenofx

# forx_i = np.zeros((len(offdia_ixsobj.offDiaDataSets[1].masterRCmotor),len(offdia_ixsobj.offDiaDataSets[1].masterRCmotor) ))
# forx_i[:,0] = offdia_ixsobj.offDiaDataSets[1].masterRCmotor
# for ii in range(4):
#     forx_i[:,ii+1] = pre_factors[ii]*np.interp(forx_i[:,0], forx[:,0], forx[:,ii+1])

# V1h = V1.T

# T = forx_i.dot(V1h)

# S = np.zeros((U1.shape[0], V1.shape[0]))
# for ii in range(2):#range(V1.shape[0]):
#     S[ii,ii] = S1[ii]

# dsf = U1.dot(S).dot(np.linalg.inv(T))

# cla()
# plot(energy1[2::], dsf[:,0])
# plot(energy1[2::], -dsf[:,1]*1.8)
# plot(energy1[2::], -dsf[:,2]*1.3)
# plot(energy1[2::], dsf[:,3]*24)
# plot(energy1[2::], -dsf[:,4]*0.85)
# plot(energy1[2::], dsf[:,5]*0.49)
# plot(energy1[2::], dsf[:,6]*2.2)
# plot(energy1[2::], -dsf[:,7]*0.45)
# plot(energy1[2::], dsf[:,8]*1.05)
# plot(energy1[2::], dsf[:,9]*1.75)
# plot(energy1[2::], dsf[:,10]*6)
# plot(energy1[2::], -dsf[:,11]*1.5)


#######################################
# decompose using SVD
#######################################

U1,S1,D1 = np.linalg.svd(SVDMatrix1)

f=h5py.File("result.h5","w")
f["energy"] = energy1
f["C1"]     = -U1[:,0]*S1[0]
f["C2"]     = -U1[:,1]*S1[1]
f["C3"]     = -U1[:,2]*S1[2]
f.close()

if(0):

    plot(energy1,-U1[:,0]*S1[0], energy1,-U1[:,1]*S1[1], energy1,-U1[:,2]*S1[2])

    # load theory
    theo = np.loadtxt('/data/id20/inhouse/data/off_dia/theory_final/Si111_1.0_1.0_-0.5.dat')

    # load diagonals
    dia0 = np.array(zip(energy1,SVDMatrix1[:,0]))
    thedata = np.loadtxt('/data/id20/inhouse/data/run2_18/run6_comm/Si_diagonals/Si_0.0_0.0_1.5_a.dat')
    dia1 = np.zeros_like(dia0)
    dia1[:,0] = energy1
    dia1[:,1] = np.interp(energy1, thedata[:,0], thedata[:,1])

    # # subract rest of elastic and background from dia1
    # from scipy.optimize import curve_fit
    # inds = np.where(np.logical_or(dia1[:,0]<=5.0 , dia1[:,0]>=85.0 ))[0]
    # popt, pcov = curve_fit(math_functions.pearson7_linear_scaling_forcurvefit, dia1[inds,0], dia1[inds,1], p0=[0.0, 1.5, 1.0, 1000, -0.1, 0.0, 1.0])
    # back = math_functions.pearson7_linear_scaling_forcurvefit(dia1[:,0], popt[0], popt[1], popt[2], popt[3], popt[4], popt[5], popt[6])
    # plot(dia1[:,0], dia1[:,1])
    # plot(dia1[:,0], back)
    # plot(dia1[:,0], dia1[:,1]-back)
    # dia1[:,1] -= back


    # # ROI 1
    # # q-point 0.96354415,  1.02352979,  0.49399393
    # thedata = np.zeros((len(energy1),4))
    # thedata[:,0] = energy1
    # thedata[:,1] = U1[:,0]*S1[0]
    # thedata[:,2] = U1[:,1]*S1[1]
    # thedata[:,3] = U1[:,2]*S1[2]

    #np.savetxt('/media/christoph/Seagate Expansion Drive/data/off_dia/offdia_results/svd_res/Si111_10_10_05_a.txt',thedata)



    #######################################
    # decompose using constrained power method
    #######################################

    #dia = np.loadtxt('/data/id20/inhouse/data/off_dia/offdia_results/Si100_00_00_05_dia.txt')
    #dia /= np.linalg.norm(dia)

    numC = 3
    W_ini = np.random.random((SVDMatrix1.shape[0],numC))
    W_ini[:,0] = SVDMatrix1[:,0]
    W_ini[:,2] = dia1[:,1]
    W_up  = np.zeros_like(W_ini)
    W_up[:,1] += 1.0
    W_up[:,2] += 0.0

    coeff_ini = np.random.random((SVDMatrix1.shape[1],numC))
    coeff_ini /= np.linalg.norm(coeff_ini)
    coeff_up  = np.ones((SVDMatrix1.shape[1],numC))
    W, coeff, err = xrs_utilities.constrained_mf(SVDMatrix1,W_ini, W_up, coeff_ini, coeff_up, maxIter=1000, tol=1.0e-8)

    thedata = np.zeros((len(energy1),4))
    thedata[:,0] = energy1
    thedata[:,1] = W[:,0]
    thedata[:,2] = W[:,1]
    thedata[:,3] = W[:,2]

    np.savetxt('/data/id20/inhouse/data/off_dia_jan18/results/Si111_terzo_5deg_asym_q0_1.0_1.0_-0.5_b.dat', thedata)

    #######################################
    # decompose using un-constrained power method (updating off-dia and dia2)
    #######################################

    numC = 3
    W_ini = np.random.random((SVDMatrix.shape[0],numC))
    W_ini[:,0] = SVDMatrix[:,0]
    W_ini[:,2] = dia
    W_up  = np.zeros_like(W_ini)
    W_up[:,1] += 1.0
    W_up[:,2] += 1.0

    coeff_ini = np.random.random((SVDMatrix.shape[1],numC))
    coeff_up  = np.ones((SVDMatrix.shape[1],numC))
    W, coeff, err = xrs_utilities.constrained_mf(SVDMatrix,W_ini, W_up, coeff_ini, coeff_up, maxIter=1000, tol=1.0e-8)

    thedata = np.zeros((len(energy),4))
    thedata[:,0] = energy
    thedata[:,1] = W[:,0]
    thedata[:,2] = W[:,1]
    thedata[:,3] = W[:,2]

    np.savetxt('/home/christoph/data/off_dia/offdia_results/pm_res/Si111_10_10_05_a.txt',thedata)









