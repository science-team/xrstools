from XRStools import  xrs_utilities 

import h5py
def saveDictH5( obj, fn):
    file = h5py.File(fn,"w")
    for k in obj.keys():
        file[k] = obj[k]
    file.close()
def loadDictH5(  fn):
    obj = {}
    file = h5py.File(fn,"r")
    for k in file:
        obj[k] = file[k] [:]
    file.close()
    return obj


import numpy as np

#    W, coeff = constrained_mf(signals,W_fixed, W_free, coeff_ini, maxIter=100, tol=1.0e-14)
def  constrained_mf(signals,W_fixed, W_free, coeff_ini, maxIter=100, tol=1.0e-14):

    if W_free is not None:
       W = np.zeros( [W_fixed.shape[0] , (W_fixed.shape[1]+W_free.shape[1])    ]   )
    else:
       W = np.zeros( [W_fixed.shape[0] , (W_fixed.shape[1])    ]   )

    W [:, 0: W_fixed.shape[1] ] = W_fixed
    if W_free is not None:
       W [:,  W_fixed.shape[1]: ] = W_free

    for i in range(10):
        ####  W *coeffs = signals
        coeffs = (np.linalg.lstsq(   W,   signals  ))[0]
        
        if W_free is None:
            return W, coeffs
    

       
        diff = signals - np.dot( W , coeffs)
        error = np.linalg.norm(diff)
        print( "ERROR MF ", error*error)
        
        reduced_signals = signals -  np.dot( W[:,: W_fixed.shape[1]   ] ,  coeffs[0:W_fixed.shape[1], : ] )
        
        W_free = (np.linalg.lstsq(  (coeffs[W_fixed.shape[1]:,: ] ).T ,   reduced_signals.T  ))[0].T
        W [:,  W_fixed.shape[1]: ] = W_free

        for i in range( W.shape[-1]   ) :
            W[:,i] = W[:,i]/np.linalg.norm(W[:,i])
    return W, coeffs
    
                                         



signals_dict = loadDictH5(  "Signals111.h5")

# coeffs_dict = loadDictH5(  "STW_infos111_allparallel.h5")
# coeffs_dict = loadDictH5(  "STW_infos111_nominal_0.130000.h5")
# coeffs_dict = loadDictH5(  "STW_infos111_nominal_0.118000.h5")
# coeffs_dict = loadDictH5(  "STW_infos111_all_parallel.h5")
#coeffs_dict = loadDictH5(  "STW_infos111_nominal_-1.000000.h5")

coeffs_dict = loadDictH5(  "STW_infos111_resynt.h5")



print( list(signals_dict.keys()) )

print( list(coeffs_dict.keys()) )

for key in ['angles_rad', 'energy', 'signals']:
    exec("%s=signals_dict['%s']"%(key,key))


coeffs_matrix = []
for key in ['A_curve', 'B_curve', 'C_curve']:
    xrad, coeffs = coeffs_dict[key]
    mycoeffs = np.interp( angles_rad,xrad,coeffs  )
    coeffs_matrix.append(mycoeffs)


# coeffs_matrix[2] = coeffs_matrix[2]
    
coeffs_matrix = np.array( coeffs_matrix )


# 21x131 =        21 x 3           3 x 131    
# signals.T = np.dot(coeffs_matrix.T , scatterings )


E0 = 7.92515
inds = np.where(np.logical_and(np.array(energy)>=7.927, np.array(energy)<=8.020))[0]
signals  = signals[inds]
energy = energy[inds]

E0 = 7.9255


dia_1_1_05_exp        = np.array(h5py.File("dia_ixsobj_aligned.h5","r") ["after_stichting_aligning"]["offDiaDataSets"]["DS0001"]["alignedSignalMatrix"][:])
dia_1_1_05_exp_energy = np.array(h5py.File("dia_ixsobj_aligned.h5","r") ["after_stichting_aligning"]["offDiaDataSets"]["DS0001"]["energy"][:]             )
dia_1_1_05_exp  = dia_1_1_05_exp.sum(axis=-1)
dia_1_1_05_exp  = np.interp( energy, dia_1_1_05_exp_energy ,   dia_1_1_05_exp   ) 

sum_res = np.trapz( (energy-E0)*dia_1_1_05_exp , x=(energy-E0) )

dia_1_1_05_exp = 2.25/sum_res * dia_1_1_05_exp


tmp = np.loadtxt("Christoph_Documents/Diagonals/Re_partie_diagonale/dia_results/Si100_15_00_00_a.txt")
dia_0_0_15_exp         = tmp[:,1]
dia_0_0_15_exp_energy  = tmp[:,0]
dia_0_0_15_exp  = np.interp( (energy-E0)*1.0e3   , dia_0_0_15_exp_energy ,   dia_0_0_15_exp   ) 

sum_res = np.trapz( (energy-E0)*dia_0_0_15_exp , x=(energy-E0) )

dia_0_0_15_exp = 2.25/sum_res * dia_0_0_15_exp

#18, 66
signals[18] = 0.5*(signals[17]+signals[19])
signals[66] = 0.5*(signals[65]+signals[67])

## signals = np.array(signals.T)

#######################################
# decompose using constrained power method
#######################################

#dia = np.loadtxt('/data/id20/inhouse/data/off_dia/offdia_results/Si100_00_00_05_dia.txt')
#dia /= np.linalg.norm(dia)

numC = 2
W_fixed = np.random.random((signals.shape[0],1))
W_fixed[:,0] = dia_1_1_05_exp

W_free  = np.random.random((signals.shape[0],1))

coeff_ini = np.random.random((numC, signals.shape[1]))
coeff_ini /= np.linalg.norm(coeff_ini)


W, coeff = constrained_mf(signals,W_fixed, W_free, coeff_ini)


W_fixed =  W[:, :W_fixed.shape[-1] ]
W_free =  W[:, W_fixed.shape[-1]: ]

thedata = np.zeros((len(energy),1+numC))
thedata[:,0] = (energy-E0)*1.0e3
thedata[:,1] = W_fixed[:,0]
thedata[:,2] = W_free[:,0]
# thedata[:,3] = W[:,2]

np.savetxt('RESFACT_w.txt', thedata)

thecoeffs = np.zeros((len(angles_rad),1+numC))
thecoeffs[:,0] = angles_rad
thecoeffs[:,1:1+numC] = coeff.T[:,0:numC]

np.savetxt('RESFACT_c.txt', thecoeffs)



for i in range (10):
    print (i)
    Wtest = np.array(W)
    Wtest[:,1] = W[:,1]-0.1*i*W[:,0]
    Wtest, coeff = constrained_mf(signals,Wtest , None, coeff_ini)


    thedata = np.zeros((len(energy),1+numC))
    thedata[:,0] = (energy-E0)*1.0e3
    thedata[:,1:] = Wtest
    
    np.savetxt('RESFACT_w_%d.txt'%i, thedata)
    
    thecoeffs = np.zeros((len(angles_rad),1+numC))
    thecoeffs[:,0] = angles_rad
    thecoeffs[:,1:1+numC] = coeff.T[:,0:numC]
    
    np.savetxt('RESFACT_c_%d.txt'%i, thecoeffs)



    

raise    

    
    
W_up[:] = 0

W, coeff, err = constrained_mf(signals,W_ini, W_up, coeff_ini, coeff_up, maxIter=1000, tol=1.0e-8)
thecoeffs[:,1:1+numC] = coeff[:,0:numC]

np.savetxt('RESFACT_c_again.txt', thecoeffs)

print(" SHAPE SIGNALS ", signals.shape)
newsig = []
for l in signals.T:
    f = 1/l.sum()
    l = l*f
    newsig.append(l)

signals = np.array(newsig).T

h5py.File("newsig.h5","w")["signals_renorm"] = signals
np.savetxt("signals_renorm.txt", signals)

sm = signals.mean(axis=-1)

signals = signals-sm[:,None]
newsig = []
for l in signals.T:
    f = 1/(l*l).sum()
    l = l*np.sqrt(f)
    newsig.append(l)
    
signals = np.array(newsig).T
h5py.File("newsig2.h5","w")["signals_renorm2"] = signals
np.savetxt("signals_renorm2.txt", signals)
