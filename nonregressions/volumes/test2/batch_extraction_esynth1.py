#  newfactors e' gia pronto , dopo verifica fuzionalita di roi_sel in ximages
import numpy as np
import h5py
import glob
import json

BATCH_PARALLELISM = 1

import os
def main():
    peaks_shifts = np.array(
                     [  6471.983002,
                        6471.318152,
                        6470.612314,
                        6471.217828,
                     ]) - 6470.6

    datadir =  "/data/scisofttmp/mirone/Loic1/data/"
    filter_path =  datadir + "../mask.h5:/filter"
    filename_template = "HC-D_2Dmap_%04d"
    data_path_template =  datadir + filename_template + ".nxs:/root.spyc.config1d_RIXS_0024/scan_data/data_03"
    monitor_path_template = datadir + filename_template +"_monitor.hd5:/monitor"

    # energy_custom_grid = np.array([6746.0,6754.0,6755.5,6756.2,6757.5,6759.3,6762.5,6770.0,6790.5])
    
    # energy_custom_grid = np.array([6744.0,6754.0,6755.5,6756.2,6757.5,6759.3,6762.5,6770.0,6792])
    
    energy_custom_grid = np.array([6744.0,6754.0,    6754.8,   6755.5,6756.2,6757.5,     6758.63,         6759.3,6762.5,6770.0,6792])

    energy_exp_grid  = h5py.File( datadir +(filename_template%1)+".nxs" ,"r")["/root.spyc.config1d_RIXS_0024/scan_data/actuator_1_1"][()]
    
    scan_interval   =  [1,476]   # from 1 to 475 included 

    Ydim = 25
    Zdim = 19
    Edim = 9

    roi_target_path          = "results/myrois.h5:/ROIS"
    reference_target_file    = "results/response.h5"
    signals_target_file      = "results/extracted.h5" 
    scalarprods_target_file  = "results/scalar_prods.h5"

    steps_to_do = {
        "do_step_make_roi":                      False,
        "do_step_make_reference":                False,
        "do_step_sample_extraction":             False,
        "do_step_scalar_products":               True, 
        "do_step_interpolation_coefficients":    False,
        "do_step_finalise_for_fit":              False
    }


    use_custom_components = None
    # use_custom_components = "components.h5"

    scans_to_use_for_roi = list(range(1,20))

    
    tools_sequencer(  peaks_shifts          = peaks_shifts          ,
                      datadir               = datadir               ,
                      filter_path           = filter_path           ,
                      filename_template     = filename_template     ,
                      data_path_template    = data_path_template    ,
                      energy_custom_grid    = energy_custom_grid    ,
                      energy_exp_grid       = energy_exp_grid       ,
                      monitor_path_template = monitor_path_template ,
                      scan_interval         = scan_interval         ,
                      scans_to_use_for_roi  = scans_to_use_for_roi  ,
                      Ydim                  = Ydim                  ,
                      Zdim                  = Zdim                  ,
                      Edim                  = Edim                  ,
                      
                      roi_target_path          = roi_target_path         ,
                      reference_target_file    = reference_target_file   , 
                      signals_target_file      = signals_target_file     , 
                      scalarprods_target_file  = scalarprods_target_file ,

                      
                      steps_to_do = steps_to_do,

                      use_custom_components = use_custom_components

    ) 



def process_input(s, go=0, exploit_slurm_mpi = 0, stop_omp = False):
    open("input_tmp_%d.par"%go, "w").write(s)
    background_activator = ""
    if (go % BATCH_PARALLELISM ):
        background_activator = "&"

    prefix=""
    if stop_omp:
        prefix = prefix +"export OMP_NUM_THREADS=1 ;"

        
    if (  exploit_slurm_mpi==0  ):
        comando = (prefix +"mpirun -n 1 XRS_swissknife  input_tmp_%d.par  %s"%(go, background_activator))
    elif (  exploit_slurm_mpi>0  ):
        comando = (prefix + "mpirun XRS_swissknife  input_tmp_%d.par  %s"%(go, background_activator) )
    else:
        comando = (prefix + "mpirun -n %d XRS_swissknife  input_tmp_%d.par  %s"%(abs( exploit_slurm_mpi  ), go, background_activator) )

    res = os.system( comando )

    assert (res==0) , " something went wrong running command : " + comando

def select_rois(  data_path_template=None, filter_path=None, roi_target_path=None, scans_to_use=None   ):
    
    inputstring = """

    create_rois_galaxies :
       expdata     :  {data_path_template}
       filter_path  :  {filter_path}
       roiaddress :       {roi_target_path}      # the target destination for rois
       scans  : {scans_to_use}

    """ .format(data_path_template = data_path_template,
                filter_path = filter_path,
                roi_target_path =  roi_target_path,
                scans_to_use = scans_to_use
    ) 
    process_input( inputstring , exploit_slurm_mpi = 0 )

def extract_sample_givenrois(
        roi_path = None,
        data_path_template = None,
        monitor_path_template = None,
        scan_interval = None,
        Ydim = None,
        Zdim = None,
        Edim = None,
        signals_target_file = None
         ):

    inputstring = """
        loadscan_2Dimages_galaxies :

             roiaddress : {roi_path}

             expdata  :  {data_path_template}

             monitor_address : {monitor_path_template}

             scan_interval    :  {scan_interval}

             Ydim : {Ydim}
             Zdim : {Zdim}
             Edim : {Edim}

             signalfile : {signals_target_file}

    """.format( roi_path = roi_path,
                data_path_template = data_path_template,
                monitor_path_template = monitor_path_template,
                scan_interval = scan_interval,
                Ydim = Ydim,
                Zdim = Zdim,
                Edim = Edim,
                signals_target_file = signals_target_file) 
    

    process_input( inputstring, exploit_slurm_mpi = 0)




def InterpInfo_Esynt_components(peaks_shifts , energy_exp_grid = None,    custom_ene_list =  None, custom_components = None ):

        components = h5py.File( custom_components ,"r")["components"] [()]
        info_dict = {}

        
        for i_interval in range(len(components)):
            info_dict[str(i_interval)]      =   {}            
            info_dict[str(i_interval)]["E"] = custom_ene_list[ i_interval   ]
            info_dict[str(i_interval)]["coefficients"]={}

            for i_n in range(len(energy_exp_grid)):
                info_dict[str(i_interval)]["coefficients"  ][  str(i_n)  ]={}
                for roi_num, de in enumerate(     peaks_shifts   ):
                    info_dict[str(i_interval)]["coefficients"  ][ str(i_n) ][ str(roi_num) ] = 0


        for ic in range(len(components)):
                
            for i_interval in range(len(custom_ene_list)-1):
                cE1 =  custom_ene_list[ i_interval   ]
                cE2 =  custom_ene_list[ i_interval+1 ]
                for i_ene, t_ene  in enumerate( energy_exp_grid)   :

                    for roi_num, de in enumerate(     peaks_shifts   ):
                        if  t_ene+de < cE1 or t_ene+de > cE2:
                            continue
                        alpha = (cE2-(t_ene+de) )/(cE2+cE1)

                        info_dict[str(ic)]["coefficients"  ][  str(i_ene)  ][  str(roi_num)  ]   += alpha * components[ic][ i_interval ]

                        
                        info_dict[str(ic)]["coefficients"  ][  str(i_ene)  ][  str(roi_num)  ]   += (1-alpha)*components[ic][ i_interval+1  ]

        return info_dict    

    
def  InterpInfo_Esynt( peaks_shifts , energy_exp_grid = None,    custom_ene_list =  None):

    info_dict = {"energy_exp_grid":list(energy_exp_grid), "de_list": list(peaks_shifts)}

    N_custom = len(custom_ene_list)
    N_data   = len( energy_exp_grid ) 
    
    for i_interval in range(len(custom_ene_list)):
        info_dict[str(i_interval)]      =   {}            
        info_dict[str(i_interval)]["E"] = custom_ene_list[ i_interval   ]
        info_dict[str(i_interval)]["coefficients"]={}

        for i_n in range(len(energy_exp_grid)):

            info_dict[str(i_interval)]["coefficients"  ][  str(i_n)  ]={}
            for roi_num, de in enumerate(     peaks_shifts   ):
                info_dict[str(i_interval)]["coefficients"  ][ str(i_n) ][ str(roi_num) ] = 0


    for i_interval in range( N_custom -1):
        
        cE1 =  custom_ene_list[ i_interval   ]
        cE2 =  custom_ene_list[ i_interval+1 ]
        
        for i_ene, t_ene  in enumerate( energy_exp_grid)   :
            
            for roi_num, de in enumerate(     peaks_shifts   ):
                if  t_ene+de < cE1 or t_ene+de > cE2:
                    continue
                alpha = (cE2-(t_ene+de) )/(cE2-cE1)

                info_dict[str(i_interval)]["coefficients"  ][  str(i_ene)  ][  str(roi_num)  ]   = alpha
                info_dict[str(i_interval+1)]["coefficients"][  str(i_ene)  ][  str(roi_num)  ] = 1-alpha
                
    return info_dict    



    def __init__(self, peaks_shifts, interp_file, source,  custom_ene_list = None):
        
        volum_list = list(interp_file[source].keys())
        scan_num_list = np.array([ int( t.split("_") [1]) for t in volum_list])
        
        ene_list      = np.array([    interp_file[source][vn]["scans"]["Scan%03d"%sn ]["motorDict"]["energy"][()] for vn,sn in zip(volum_list, scan_num_list   )   ])

        print ( " ecco la scannumlist " , scan_num_list)
        print (" ecco ene_list", ene_list)
        
        
        self.volum_list    =  volum_list
        self.scan_num_list =  scan_num_list
        self.ene_list      =  ene_list

        order = np.argsort(  self.ene_list    )
        
        self.ene_list  = self.ene_list [order]

        if custom_ene_list is None:
            self.custom_ene_list      = self.ene_list
        else:
            self.custom_ene_list   = custom_ene_list

        self.scan_num_list  = self.scan_num_list [order]
        self.volum_list  = [ self.volum_list [ii]  for ii in order  ] 
        
        self.interp_file=interp_file
        self.source= source
        self.peaks_shifts=peaks_shifts

# interpolation_infos_file = "interpolation_infos.json"

# info_dict={}
# for i in range(NC):
#     dizio = {}
#     info_dict[str(i)] = {"coefficients":dizio}
#     c = model.components_[i]
#     np = len(c)
#     for j in range(np):
#         dizio[str(j)] = float(c[j])

# json.dump(info_dict,open( interpolation_infos_file,"w"), indent=4)


        
    def interpola_Esynt(self,  roi_sel=roi_sel ):
        print ( " ECCO I DATI ")
        print (  self.ene_list  ) 
        print (  self.peaks_shifts   )

        info_dict = {}

        for i_intervallo in range(len(self.custom_ene_list)):
            info_dict[str(i_intervallo)]      =   {}            
            info_dict[str(i_intervallo)]["E"] = self.custom_ene_list[ i_intervallo   ]
            info_dict[str(i_intervallo)]["coefficients"]={}
            for t_vn, t_sn, t_ene in list(zip(self.volum_list,  self.scan_num_list, self.ene_list    )):
                info_dict[str(i_intervallo)]["coefficients"  ][  t_vn  ]={}

        for i_intervallo in range(len(self.custom_ene_list)-1):
            cE1 =  self.custom_ene_list[ i_intervallo   ]
            cE2 =  self.custom_ene_list[ i_intervallo+1 ]
            for t_vn, t_sn, t_ene in list(zip(self.volum_list,  self.scan_num_list, self.ene_list    ))[0:]:
                for roi_num, de in enumerate(     self.peaks_shifts   ):
                    if roi_num not in roi_sel:
                        continue
                    if  t_ene+de < cE1 or t_ene+de > cE2:
                        continue
                    alpha = (cE2-(t_ene+de) )/(cE2-cE1)

                    info_dict[str(i_intervallo)]["coefficients"  ][  str(t_vn)  ][  str(roi_num)  ]   = alpha
                    info_dict[str(i_intervallo+1)]["coefficients"][  str(t_vn)  ][  str(roi_num)  ] = 1-alpha

        return info_dict    

    
def get_reference( roi_path = None, reference_target_file = None ):
    inputstring = """

    loadscan_2Dimages_galaxies_foilscan :
        roiaddress : {roi_path}           
        expdata    :  None  
        signalfile : {reference_target_file}  # Target file for the signals
    """ .format( roi_path = roi_path,  reference_target_file = reference_target_file ) 

    process_input( inputstring , exploit_slurm_mpi = 0) 



def get_scalars( iE = None,
                 signals_file      = None,
                 reference_file    = None,
                 target_file    = None
):

    inputstring = """
    superR_scal_deltaXimages_Esynt :
         sample_address : {signals_file}:/E{iE}
         delta_address : {reference_file}:/Scan0
         load_factors_from : 
         nbin : 1
         target_address : {target_file}:/{iE}/scal_prods
    """ . format( iE = iE,
                  signals_file      = signals_file      ,
                  reference_file    = reference_file    ,
                  target_file    = target_file,
    )
    process_input( inputstring, exploit_slurm_mpi = 0)    


    
def get_volume_Esynt( scalarprods_file = None, interpolation_file = None):
    
    os.system("mkdir DATASFORCC")

    inputstring = """    
     superR_getVolume_Esynt :
        scalprods_address : {scalarprods_file}:/
        target_address : {scalarprods_file}:/data_for_volumes
        dict_interp : {interpolation_file}
        debin : [1, 1]
        output_prefix : DATASFORCC/test0_
    """.format( scalarprods_file = scalarprods_file ,
                interpolation_file = interpolation_file
    )     
    process_input( inputstring,  exploit_slurm_mpi = 0)

    
def myOrder(tok):
    if("volume" not  in tok):
        tokens = tok.split("_")
        print( tokens)
        return int(tokens[1])*10000+ int(tokens[2])
    else:
        return 0
    
def reshuffle(   volumefile  = "volumes.h5",   nick = None    ):

    h5file_root = h5py.File( volumefile ,"r+" )
    h5file = h5file_root[nick]
    scankeys = list( h5file.keys())
    scankeys.sort(key=myOrder)
    print( scankeys) 
    
    volumes = []
    for k in scankeys:
        if k[:1]!="_":
            continue
        print( k)
        if "volume" in h5file[k]:
            volumes.append( h5file[k]["volume"]  )
    # volume = np.concatenate(volumes,axis=0)
    volume = np.array(volumes)
    if "concatenated_volume" in h5file:
        del h5file["concatenated_volume"]
    h5file["concatenated_volume"]=volume
    h5file_root.close()
    
## THE FOLLOWING PART IS THE RELEVANT ONE


def tools_sequencer(  peaks_shifts = None,
                      datadir = None,
                      filter_path = None,
                      filename_template = None,
                      data_path_template = None,
                      energy_custom_grid = None,
                      energy_exp_grid  = None,
                      monitor_path_template = None,
                      scan_interval = None,
                      scans_to_use_for_roi = None,
                      Ydim = None,
                      Zdim = None,
                      Edim = None, 
                      roi_target_path          = None,
                      reference_target_file    = None,
                      signals_target_file      = None,
                      scalarprods_target_file  = None,
                      steps_to_do = None,
                      use_custom_components = None
) :


    
    if(steps_to_do["do_step_make_roi"]):   # ROI selection and reference scan
        select_rois(  data_path_template = data_path_template, filter_path = filter_path, roi_target_path = roi_target_path, scans_to_use=scans_to_use_for_roi   )
    roi_path = roi_target_path


    if(steps_to_do["do_step_make_reference"]):  
        get_reference( roi_path = roi_path ,    reference_target_file = reference_target_file   )
    reference_file = reference_target_file



    if(steps_to_do["do_step_sample_extraction"]): # SAMPLE extraction
        extract_sample_givenrois(
            roi_path              = roi_path               ,
            data_path_template    = data_path_template     ,
            monitor_path_template = monitor_path_template  ,

            scan_interval      = scan_interval      ,
            Ydim               = Ydim               ,
            Zdim               = Zdim               ,
            Edim               = Edim               ,
            signals_target_file = signals_target_file 
        )
    signals_file = signals_target_file    




    if(steps_to_do["do_step_scalar_products"]):
        os.system("rm %s"%scalarprods_target_file)
        for iE in range(Edim) :
            get_scalars(  iE = iE,
                          signals_file = signals_file,
                          reference_file = reference_file,
                          target_file =  scalarprods_target_file
            )

    scalarprods_file = scalarprods_target_file


    interpolation_infos_file = "interpolation_infos.json"
    if(steps_to_do["do_step_interpolation_coefficients"]):    # INTERPOLATION  ESYNTH
        if use_custom_components is None:        
            info_dict = InterpInfo_Esynt(  peaks_shifts ,
                                           energy_exp_grid = energy_exp_grid,
                                           custom_ene_list =  energy_custom_grid
            )
        else:
            info_dict = InterpInfo_Esynt_components(  peaks_shifts,
                                                      energy_exp_grid = energy_exp_grid,
                                                      custom_ene_list =  energy_custom_grid,
                                                      custom_components = use_custom_components
            )
        json.dump(info_dict,open( interpolation_infos_file,"w"), indent=4)



    
    # ### ESYNTH
    if(steps_to_do["do_step_finalise_for_fit"]):
        get_volume_Esynt( scalarprods_file = scalarprods_file,
                          interpolation_file = interpolation_infos_file)



main()
