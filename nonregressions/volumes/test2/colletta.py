from numpy import *
import h5py

f=h5py.File("results/extracted.h5","r")
res=[ [0 for i in range(9) ] for r in range(5)]

for ie in range(9):
    
    he = f["E%d"%ie]

    for iscan in range(19):
        for ir in range(4):

            m = he["Scan%d/%02d/matrix"%(iscan, ir)][()]
            res[ir+1][ie] += m.sum()

h5py.File("plot.h5","w")["plot"] = array( res) 

R = array(res)
R[0,:] = array( [        6745.981087876128,
        6754.004616913128,
        6755.549616942595,
        6756.206827194552,
        6757.550552579804,
        6759.304498100658,
        6762.542644309955,
        6770.0295586460825,
        6790.5063595030115
    ])

savetxt(  "plot.txt" , R.T ) 
