import numpy as np
import h5py
import glob
import json
import os
import h5py
import math

from XRStools import tools_sequencer_esynth_galaxies
from XRStools import xrs_read,  xrs_rois

import os



def main():
    os.system("mkdir results")
    
    peaks_shifts = np.array(
                     [  0.0,
                        0.0,
                        0.0,
                        0.0,
                     ]) - 0.0

    dataprefix = "/data/raffaela/" 
    datadir =  dataprefix + "GS9_dataGalaxies/"
    filter_path =  dataprefix + "mymask.h5:/mymask"
    filename_template = "GS9_%05d_01"
    data_path_template =  datadir + filename_template + ".nxs:/root_spyc_config1d_RIXS_00001/scan_data/data_07"
    reference_data_path_template =  dataprefix + "kapton_%05d_01" + ".nxs:/root_spyc_config1d_RIXS_00001/scan_data/data_07"

    
    monitor_path_template = None
    # monitor_path_template = datadir + filename_template +"_monitor.hd5:/monitor"

    print( datadir +(filename_template%1)+".nxs" ) 
    energy_exp_grid  = h5py.File( datadir +(filename_template%1)+".nxs" ,"r")["/root_spyc_config1d_RIXS_00001/scan_data/actuator_1_1"][()]
    
    energy_custom_grid = np.array( energy_exp_grid  )
    
    energy_custom_grid [ 0 ] -= 0.1
    energy_custom_grid [+1 ] -= 0.1
    
    scan_interval   =  [1,257]   # from 1 to 475 included 

    Ydim = 16
    Zdim = 16
    Edim = 19

    reference_scan_list =  [1]
    
    custom_components_file = None
    # custom_components_file = "components.h5"
    roi_scan_num = list(range(1,3))

    reference_clip = None
    reference_clip = [ 0, 200 ]

    ## in the reference scan for each position there is a spot with a maximum. We set zero the background which is further than
    ## such radius from the maximum
    isolate_spot_by = 7

    #### For the fit of the response function based on reference scans
    response_fit_options = dict( [
        ["niter_optical" , 40],
        ["beta_optical"  , 0.1],
        ["niter_global"  , 1  ]
    ])
    resynth_z_square = 0
    selected_rois = list(range(0,4) )
    
    steps_to_do = {
        "do_step_make_roi":                      False,
        "do_step_sample_extraction":             False,
        
        "do_step_extract_reference_scan":        False,
        "do_step_fit_reference_response":        False,
        "do_step_resynthetise_reference":        False,
        
        "do_step_scalar":               False, 
        "do_step_interpolation_coefficients":    False,
        "do_step_finalise_for_fit":              True
    }

    scalar_products_target_file  = "results/scalar_prods.h5"

    roi_target_path          = "results/myrois.h5:/ROIS"
    reference_target_file    = "results/response.h5"
    signals_target_file      = "results/extracted.h5" 

    extracted_reference_target_file = "results/reference.h5"
    response_target_file = "results/response.h5"


    interpolation_infos_file = "interpolation_infos.json"
    resynthetised_reference_and_roi_target_file = "results/resynthetised_roi_and_scan.h5"
    

    
    tools_sequencer_esynth_galaxies.tools_sequencer(  peaks_shifts          = peaks_shifts          ,
                      filter_path           = filter_path           ,
                      roi_scan_num          = roi_scan_num          ,
                      roi_target_path       = roi_target_path       ,
                      data_path_template    = data_path_template    ,
                      reference_data_path_template = reference_data_path_template,
                      
                      steps_to_do = steps_to_do,
                      
                      scan_interval         = scan_interval         ,                      
                      Ydim                  = Ydim                  ,
                      Zdim                  = Zdim                  ,
                      Edim                  = Edim                  ,

                      monitor_path_template = monitor_path_template ,
                      signals_target_file = signals_target_file,


                      reference_scan_list = reference_scan_list,
                      reference_clip = reference_clip,
                      extracted_reference_target_file    = extracted_reference_target_file   ,

                      isolate_spot_by =  isolate_spot_by,
                      response_target_file = response_target_file,
                      response_fit_options = response_fit_options,
                      
                      resynthetised_reference_and_roi_target_file = resynthetised_reference_and_roi_target_file,
                      resynth_z_square = resynth_z_square,
                      selected_rois = selected_rois,
                      
                      scalar_products_target_file  = scalar_products_target_file ,

                    
                      energy_custom_grid    = energy_custom_grid    ,
                      custom_components_file = custom_components_file,
                      interpolation_infos_file =  interpolation_infos_file,
                      energy_exp_grid = energy_exp_grid
    ) 



main()
