import numpy as np
import h5py
import glob
import json
import os
import h5py
import math

from XRStools import tools_sequencer_esynth
from XRStools import xrs_read,  xrs_rois

import os
def main():    
    os.system("xz -dk ../mask.h5.xz --stdout > mask.h5")

    filter_path = "mask.h5:/FILTER_MASK/filter"

    roi_scan_num   = [245,246,247]
    reference_scan_list = [245, 246, 247]

    monitor_column = "izero/0.000001"
    
    first_scan_num = 651 
    Ydim        = None # not used. it is the scan lenght , it is given by the data, contained in the scan
    Zdim        = 10
    Edim        = 7

    elastic_scan_for_peaks_shifts = 42

    energy_custom_grid = [  2*13.253006- 13.25551 , 13.253006,  13.25551,   13.258008, 13.260506 , 13.263004, 13.268   ]
    
    ### custom_components_file = "abc.h5"
    custom_components_file = None
    
    datadir =  "/data/id20/inhouse/data/run3_20/run3_es949"
    #    reference_clip = None
    reference_clip = [ 90, 180 ]

    isolate_spot_by = 6

    response_fit_options = dict( [
        ["niter_optical" , 100],
        ["beta_optical"  , 0.1],
        ["niter_global"  , 3  ]
    ])

    resynth_z_square = 0
    
    selected_rois = list(range(0,72))
    
    steps_to_do = {
        "do_step_make_roi":                      False,
        "do_step_sample_extraction":             False,
        "do_step_extract_reference_scan":             False,                
        "do_step_fit_reference_response":               False,
        "do_step_resynthetise_reference":               False,

        "do_step_scalars"               :    False,
    
        "do_step_interpolation_coefficients":    False,
        
        "do_step_finalise_for_fit":              True
    }

    
    os.system("mkdir results")
    
    scalar_products_target_file  = "results/scalar_products.h5"
    
    roi_target_path    = "results/myrois.h5:/ROIS"
        
    signals_target_file = "results/signals.h5"
    
    extracted_reference_target_file = "results/reference.h5"
    
    response_target_file = "results/response.h5"

    resynthetised_reference_and_roi_target_file = "results/resynthetised_roi_and_scan.h5"

    interpolation_infos_file =  "interpolation_infos.json"

    ###########################################################################################
    ######  LOADING PEAKS SHIFTS
    ###### peaks_shifts = h5py.File("../peaks_positions_for_analysers.h5","r")["peaks_positions"][()]
    ###### assert( len(peaks_shifts) == 72)
    
    if steps_to_do["do_step_interpolation_coefficients"]:
        roiob = xrs_rois.roi_object()
        roiob.loadH5( roi_target_path  )
        elastic = xrs_read.Hydra( datadir )
        elastic.set_roiObj( roiob )
        elastic.get_compensation_factor( elastic_scan_for_peaks_shifts , method='sum')
        el_dict = elastic.cenom_dict
        Enominal = np.median(  list( el_dict.values() ) )
        peaks_shifts = np.array([    el_dict["ROI%02d"%i] if ("ROI%02d"%i) in  el_dict else nan    for i in range( 72)  ] ) 
        peaks_shifts -= Enominal
    else:
        peaks_shifts = None
    
    ##############################################################
    ##########################################################################

    tools_sequencer_esynth.tools_sequencer(
        peaks_shifts          = peaks_shifts          ,
                      datadir               = datadir               ,
                      filter_path           = filter_path           ,
                      roi_scan_num          = roi_scan_num          ,
                      roi_target_path       = roi_target_path       ,
                      
                      steps_to_do = steps_to_do,
                      
                      first_scan_num =  first_scan_num,
                      Ydim        =  Ydim       ,
                      Zdim        =  Zdim       ,
                      Edim        =  Edim       ,

                      monitor_column = monitor_column,
                      signals_target_file = signals_target_file,

                      reference_scan_list = reference_scan_list,
                      reference_clip = reference_clip,
                      extracted_reference_target_file = extracted_reference_target_file ,
                      isolate_spot_by =  isolate_spot_by,
                      response_target_file = response_target_file,
                      response_fit_options = response_fit_options,
                      
                      resynthetised_reference_and_roi_target_file = resynthetised_reference_and_roi_target_file,
                      resynth_z_square = resynth_z_square,

                      selected_rois = selected_rois,
                      
                      scalar_products_target_file      = scalar_products_target_file ,
                      
                      energy_custom_grid =  energy_custom_grid,
                      custom_components_file = custom_components_file,
                      interpolation_infos_file =  interpolation_infos_file
            
                      
    )
    
    
            
            
main()


